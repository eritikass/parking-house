import {
	assert
} from 'chai';
import moment from 'moment';

import testData from './priceTestData.json';

import priceCalc from '../api/helpers/priceCalc';

describe('priceCalc', function() {

	testData.forEach(function(data) {
		Object.keys(data.sum).forEach(function(clientType) {
			const sum = data.sum[clientType];
			it(`[${clientType}] ${data.startDate} => ${data.endDate} >> ${JSON.stringify(data.count)} >> ${sum}`, function() {
				const startDate = moment(data.startDate).toDate();
				const endDate = moment(data.endDate).toDate();

				const priceData = priceCalc(startDate, endDate, clientType);

				assert.deepEqual(data.count, priceData.count);
				assert.equal(sum, priceData.sum);
			});
		});
	});

});
