import {
	assert
} from 'chai';
import moment from 'moment';

import {
	client,
	parking
} from '../api/helpers/parse';

const dataClient = {
	"clientId": "e6a85759-318c-4138-8991-bc763010551e",
	"type": "premium"
};

const dataParking = {
  "client": dataClient,
  "startTime": "2017-09-22T10:20:50",
  "endTime": "2017-09-22T13:40:50",
  "parkingHouse": "default"
};

const dataParkingResult = {
  "client": dataClient,
  "startTime": moment("2017-09-22T10:20:50").toDate(),
  "endTime": moment("2017-09-22T13:40:50").toDate(),
  "parkingHouse": "default"
};

describe('parse', function() {
	it('client', function() {
		assert.deepEqual(dataClient, client(dataClient));
	});
	it('parking', function() {
		assert.deepEqual(dataParkingResult, parking(dataParking));
	});
});
