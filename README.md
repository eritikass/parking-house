# invoicing system for a parking house

## Setup

Make sure you have [docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/) installed.

Simply run (add `-d` to run in background):
``` bash
docker-compose up
```

connect into container
``` bash
docker-compose exec node bash
```

Open browser:
  * [swagger-ui for api testing](http://localhost:3856/)

Mongo can be accessed `localhost:28017`

## Mock data

There is helper script added to mock data, one customer with random type and up to 20 records

``` bash
yarn run mockdata
```

## Api client

NPM package for our api (generated using [swagger-codegen](https://github.com/swagger-api/swagger-codegen)):
  * https://www.npmjs.com/package/@eritikass/paring-house-api
  * https://gitlab.com/eritikass/paring-house-api

## Example data

*parking object (for `POST`: `/parking` testing):*
``` json
{
  "client": {
    "clientId": "e6a85759-318c-4138-8991-bc763010551e",
    "type": "premium"
  },
  "startTime": "2017-09-22T10:20:50",
  "endTime": "2017-09-22T13:30:50",
  "parkingHouse": "default"
}
```
