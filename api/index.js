import path from 'path';
import swaggerExpressMW from 'swagger-express-mw';
import express from 'express';

import responseError from './helpers/responseError';
import './helpers/tz';

const app = express();
const PORT = process.env.PORT || 3857;

const apiConfig = {
  appRoot: path.resolve(__dirname),
  swaggerFile: path.resolve(__dirname, '../swagger.yml')
};

function routeNotFound(req, res) {
  responseError(res, 'Route Not Found', 404);
}

swaggerExpressMW.create(apiConfig, function (err, swaggerExpress) {
  if (err) { throw err; }
  // install middleware
  swaggerExpress.register(app);

  // route not found
  app.use(routeNotFound);
});



app.listen(PORT, '0.0.0.0', function () {
  console.log(`Example app listening on port ${PORT}!`);
});
