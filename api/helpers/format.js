import moment from 'moment';

export const parking = (mongoRecord) => {
  return {
    "client": {
      "clientId": mongoRecord.client.clientId,
      "type": mongoRecord.client.type
    },
    "startTime": moment(mongoRecord.startTime).format(),
    "endTime": moment(mongoRecord.endTime).format(),
    "parkingHouse": mongoRecord.parkingHouse,
    "parkingId": mongoRecord._id,
    "cost": mongoRecord.costData.sum
  };
};
