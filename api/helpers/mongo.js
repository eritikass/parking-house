import {
  MongoClient
} from 'mongodb';

// TODO: move to config
const MONGO_URI = 'mongodb://mongo:27017/parking_house';

export const collectionNameParking = 'parking';

export const getConnection = (collectionName, cb) => {
  MongoClient.connect(MONGO_URI, function(err, db) {
    if (err) {
      return cb(err);
    }
    const collection = db.collection(collectionName);
    cb(null, collection);
  });
};

export const insert = (data, collectionName, cb) => {
  getConnection(collectionName, function(err, collection) {
    if (err) {
      return cb(err);
    }
    collection.insert(data, cb);
  });
};

export const find = (query, collectionName, cb) => {
  getConnection(collectionName, function(err, collection) {
    if (err) {
      return cb(err);
    }
    collection.find(query).toArray(cb);
  });
};

export const aggregate = (options, collectionName, cb) => {
  getConnection(collectionName, function(err, collection) {
    if (err) {
      return cb(err);
    }
    collection.aggregate(options, cb);
  });
};
