import {
  anyNonNil
}  from 'is-uuid';
import moment from 'moment';

export const isUUID = anyNonNil;

export const client = (clientData) => {
  if (!clientData) {
    throw new Error("no client data");
  }

  if (!isUUID(clientData.clientId)) {
    throw new Error("clientId is not valid UUID");
  }

  if (clientData.type != 'premium' && clientData.type != 'regular') {
    throw new Error("clientId type is not valid!");
  }

  return {
    'clientId': clientData.clientId,
    'type': clientData.type
  };
};

export const parking = (data) => {
  const clientData = client(data.client);

  const endTime = moment(data.endTime);
  const startTime = moment(data.startTime);

  if (!endTime.isValid() || !startTime.isValid()) {
    throw new Error("bad date data");
  }

  if (startTime.isAfter(endTime)) {
    throw new Error("start time need to be before end time");
  }

  // FIXME: for testing
  if (endTime.diff(startTime, 'hours') > 24) {
    throw new Error("just for demo atm - no period more than 24h allowed!");
  }

  return {
    client: clientData,
    endTime: endTime.toDate(),
    startTime:  startTime.toDate(),
    parkingHouse: data.parkingHouse || "default"
  };
};
