const responseError = (res, message, code = 400, type = 'error') => {
  res
    .status(code)
    .json({
      code,
      type,
      message
    });
};

export default responseError;
