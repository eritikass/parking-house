import moment from 'moment';

const prices = {
  "regular": {
    day: 1.5,
    night: 1
  },
  "premium": {
    day: 1,
    night: 0.75
  }
};

const priceCalc = (startDate, endDate, clientType) => {
  const oStartTime = moment(startDate);
  const oEndTime = moment(endDate);

  let count = {
    day: 0,
    night: 0
  };

  do {
    // TODO: move to config? new method next line
    const priceMode = (oStartTime.hour() >= 7 && oStartTime.hour() < 19) ? 'day' : 'night';

    count[priceMode]++;

    // start next half hour
    oStartTime.add('30', 'minutes');
  } while (oEndTime.isAfter(oStartTime));

  if (!prices[clientType]) {
    throw new Error(`no prices found for ${clientType}`);
  }
  const c = prices[clientType];

  return {
    count,
    sum: (count.day * c.day) + (count.night * c.night)
  };
};

export default priceCalc;
