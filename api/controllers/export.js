import path from 'path';

import Twig from 'twig';
import pdf from 'html-pdf';
import moment from 'moment';

import {
    find,
    collectionNameParking
} from '../helpers/mongo';
import responseError from '../helpers/responseError';

const template = path.resolve(__dirname, '../templates/export.twig');

const pdfCreateOptions = {
//  width: '50mm',
//  height: '90mm',
};

/**
 * @param {request} req
 * @param {response} res
 */
export const get = (req, res) => {
  const clientId = req.swagger.params.clientId.raw;
  const year = parseInt(req.swagger.params.year.raw, 10);
  const month = parseInt(req.swagger.params.month.raw, 10);

  const date = moment()
    .date(1)
    // month count starts from 0
    .month(month-1)
    .year(year);

  if (!date.isValid()) {
    return responseError(res, 'invalid date data', 400);
  }

  const filename = `invoice_${year}_${month}.pdf`;

  const filter = {
    'client.clientId': clientId,
    // XXX: select only by start time?
    startTime: {
      $gte: date.startOf('month').toDate(),
      $lt: date.endOf('month').toDate()
    }
  };

  // FIXME: callback hell
  find(filter, collectionNameParking, function(err, result) {
    if (err) {
      console.error(err);
      return responseError(res, 'error getting data to db', 400);
    }

    if (result.length === 0) {
      // FIXME: we should probably still print invoice
      // for example premium user still gets his monthly bill
      return responseError(res, 'no data found for given month', 400);
    }

    const data = {
      dateCreated: moment().format('LL'),
      total: 0
    };

    data.items = result.map((parking) => {
      data.client = parking.client;
      data.total += parking.costData.sum;
      return {
        // FIXME: show date better formated?
        name: moment(parking.startTime).format('HH:mm')
          + ' >> '
          + moment(parking.endTime).format('HH:mm')
          + ' (' +  moment(parking.startTime).format('dddd, Do') + ') ',
        price: parking.costData.sum
      };
    });

    // FIXME: this code should not be here!!!!
    // move to module of his own
    if (data.client.type === 'premium') {
      data.items.push({
        name: 'Premium customer monthly fee',
        price: 20
      });

      data.total += 20;

      data.total = Math.min(300, data.total);
    }

    Twig.renderFile(template, data, (err, html) => {
      if (err) {
        console.error(err);
        return responseError(res, 'twig render error', 400);
      }
      pdf.create(html, pdfCreateOptions).toStream(function(err, stream) {
        if (err) {
          console.error(err);
          return responseError(res, 'pdf create error', 400);
        }
        res.setHeader('Content-disposition', 'attachment; filename="' + filename + '"');
        res.setHeader('Content-type', 'application/pdf');
        stream.pipe(res);
      });
    });
  });

};
