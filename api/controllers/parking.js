import {
    insert as mongoInsert,
    collectionNameParking
} from '../helpers/mongo';

import {
  parking as parseParking
} from '../helpers/parse';
import priceCalc from '../helpers/priceCalc';
import {
  parking as parkingFormat
} from '../helpers/format';

import responseError from '../helpers/responseError';

/**
 * @param {request} req
 * @param {response} res
 */
export const post = (req, res) => {
  const p = req.swagger.params.parking.raw;
  let pData = null;

  try {
    pData = parseParking(p);
  } catch (e) {
    console.error(e);
    return responseError(res, e.message, 400);
  }

  pData.costData = priceCalc(pData.startTime, pData.endTime, pData.client.type);

  mongoInsert(pData, collectionNameParking, function (err, result) {
    if (err) {
      console.error(err);
      return responseError(res, 'error adding data to db', 400);
    }
    res.json(parkingFormat(result.ops[0]));
  });


};
