import {
    find,
    collectionNameParking
} from '../helpers/mongo';
import {
  parking as parkingFormat
} from '../helpers/format';
import responseError from '../helpers/responseError';

/**
 * @param {request} req
 * @param {response} res
 */
export const get = (req, res) => {
  const clientId = req.swagger.params.clientId.raw;

  find({'client.clientId': clientId}, collectionNameParking, function(err, result) {
    if (err) {
      console.error(err);
      return responseError(res, 'error getting data to db', 400);
    }
    res.json(result.map((r) => parkingFormat(r)));
  });
};
