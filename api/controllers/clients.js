import {
  aggregate,
  collectionNameParking
} from '../helpers/mongo';

import responseError from '../helpers/responseError';

/**
 * @param {request} req
 * @param {response} res
 */
export const get = (req, res) => {
  aggregate([
    {$match: {}},
    {$group:
      {
        _id: '$client'
      }
    }
  ], collectionNameParking, function (err, result) {
    if (err) {
      console.error(err);
      return responseError(res, 'error adding data to db', 400);
    }
    res.json(result.map((r) => r._id));
  });
};
