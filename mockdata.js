/* eslint import/no-commonjs: 0 */
const paringHouseApi = require('@eritikass/paring-house-api');
const moment = require('moment');
const Faker = require('Faker');
const uuidv4 = require('uuid/v4');

const apiInstance = new paringHouseApi.ParkingApi();

const count = Faker.random.number(20);

const clientId = uuidv4();
const clientType = Faker.random.arrayElement(['regular', 'premium']);

for (let i = 0; i < count; i++) {
  const oDate = moment(Faker.date.recent(40));
  const startTime = oDate.format();
  // add max 1 day as parking period (24h * 60min)
  const endTime = oDate.add('minutes', Faker.random.number(24*60)).format();

  const parking = {
    "client": {
      "clientId": clientId,
      "type": clientType
    },
    "startTime": startTime,
    "endTime": endTime,
    "parkingHouse": "default"
  };

  apiInstance.parkingPost(parking, function(error, data) {
    if (error) {
      console.error(error);
    } else {
      console.log('API called successfully. Returned data: ', data);
    }
  });
}
