#!/usr/bin/env bash

# download swagger-codegen-cli.jar (if not present)
wget -nc --progress=bar:force http://central.maven.org/maven2/io/swagger/swagger-codegen-cli/2.2.3/swagger-codegen-cli-2.2.3.jar -O swagger-codegen/cli.jar | true

apt-get update
apt-get install jq -y
jq --version

rm -fr swagger-codegen-gen
mkdir -p swagger-codegen-gen

# https://github.com/swagger-api/swagger-codegen/tree/master/modules/swagger-codegen/src/main/resources/Javascript
# https://github.com/swagger-api/swagger-codegen/blob/master/modules/swagger-codegen/src/main/java/io/swagger/codegen/languages/JavascriptClientCodegen.java

# generate swagger.json
java -jar swagger-codegen/cli.jar generate \
	--input-spec $(pwd)/swagger.yml \
	--config $(pwd)/swagger-codegen/config.json \
	--output $(pwd)/swagger-codegen-gen \
	--lang javascript


## FIXME: swagger codegen template should be actually changed to get those results!!!

jq -s '.[0] * .[1]' swagger-codegen-gen/package.json swagger-codegen/repository.json >swagger-codegen-gen/package_tmp.json
mv swagger-codegen-gen/package_tmp.json swagger-codegen-gen/package.json

cp swagger-codegen/.gitlab-ci.yml swagger-codegen-gen/.gitlab-ci.yml
