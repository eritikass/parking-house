#!/usr/bin/env bash

# install dependencies
yarn

# run project (for dev)
function run_dev {
  yarn run api_dev
}

# run project for production mode
function run_production {
  yarn run api_build
  yarn run api_start
}

#run_dev
run_production
